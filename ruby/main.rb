require 'json'
require 'socket'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

puts "I'm #{bot_name} and connect to #{server_host}:#{server_port}"

class NoobBot
  def initialize(server_host, server_port, bot_name, bot_key)
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    tcp.puts join_message(bot_name, bot_key)
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    switch = false
    track_layout = {}
    known_location = 0
    while json = tcp.gets
      throttle = 0.64
      message = JSON.parse(json)
      msgType = message['msgType']
      msgData = message['data']
      case msgType
        when 'carPositions'
          if switch
            puts "Switching lane to right"
            tcp.puts switch_message("Right")
            switch = false
            #[{"id"=>{"name"=>"Lahnat", "color"=>"red"}, "angle"=>0.0, "piecePosition"=>{"pieceIndex"=>0, "inPieceDistance"=>0.0, "lane"=>{"startLaneIndex"=>0, "endLaneIndex"=>0}, "lap"=>0}}]
          else
            current_piece = message['data'].first["piecePosition"]["pieceIndex"]
            if current_piece != known_location
              if piece_is_straigh(track_layout[current_piece])
                curve_or_straight = "straight"
              else
                curve_or_straight = "curve with angle #{track_layout[current_piece]['angle']}"
              end
              if current_piece < track_layout.size-1
                if piece_is_straigh(track_layout[current_piece+1])
                  throttle = 1.0
                elsif track_layout[current_piece+1]["angle"].to_f.abs < 45.0
                  throttle = 0.88
                end
              else
                throttle = 1.0
              end
              puts "Now on piece #{current_piece}, which is #{curve_or_straight} with throttle #{throttle}"
              known_location = current_piece
            end
            tcp.puts throttle_message(throttle)
          end
        else
          case msgType
            when 'gameInit'
              track_layout = msgData["race"]["track"]["pieces"]
              puts "TRACK: #{track_layout}"
              #{"race"=>{"track"=>{"id"=>"keimola", "name"=>"Keimola", "pieces"=>[{"length"=>100.0}, {"length"=>100.0}, {"length"=>100.0}, {"length"=>100.0, "switch"=>true}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>200, "angle"=>22.5, "switch"=>true}, {"length"=>100.0}, {"length"=>100.0}, {"radius"=>200, "angle"=>-22.5}, {"length"=>100.0}, {"length"=>100.0, "switch"=>true}, {"radius"=>100, "angle"=>-45.0}, {"radius"=>100, "angle"=>-45.0}, {"radius"=>100, "angle"=>-45.0}, {"radius"=>100, "angle"=>-45.0}, {"length"=>100.0, "switch"=>true}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>200, "angle"=>22.5}, {"radius"=>200, "angle"=>-22.5}, {"length"=>100.0, "switch"=>true}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"length"=>62.0}, {"radius"=>100, "angle"=>-45.0, "switch"=>true}, {"radius"=>100, "angle"=>-45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"radius"=>100, "angle"=>45.0}, {"length"=>100.0, "switch"=>true}, {"length"=>100.0}, {"length"=>100.0}, {"length"=>100.0}, {"length"=>90.0}], "lanes"=>[{"distanceFromCenter"=>-10, "index"=>0}, {"distanceFromCenter"=>10, "index"=>1}], "startingPoint"=>{"position"=>{"x"=>-300.0, "y"=>-44.0}, "angle"=>90.0}}, "cars"=>[{"id"=>{"name"=>"Lahnat", "color"=>"red"}, "dimensions"=>{"length"=>40.0, "width"=>20.0, "guideFlagPosition"=>10.0}}], "raceSession"=>{"laps"=>3, "maxLapTimeMs"=>60000, "quickRace"=>true}}}
            when 'join'
              puts 'Joined'
            when 'gameStart'
              puts 'Race started'
              switch = true
            when 'crash'
              puts 'Someone crashed'
            when 'gameEnd'
              puts 'Race ended'
            when 'error'
              puts "ERROR: #{msgData}"
          else
            puts "Unknown: #{msgType}: #{msgData}"
          end
          puts "Got #{msgType}"
          tcp.puts ping_message
      end
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def switch_message(direction)
    make_msg("switchLane", direction)
  end

  def make_msg(msgType, data)
    JSON.generate({:msgType => msgType, :data => data})
  end

  def piece_is_straigh(piece)
    piece["radius"].nil?
  end
end

NoobBot.new(server_host, server_port, bot_name, bot_key)
